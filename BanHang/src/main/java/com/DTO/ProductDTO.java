package com.DTO;

import org.modelmapper.ModelMapper;

import com.model.Category;
import com.model.Product;

public class ProductDTO {
	
	private int id;
	
	private String productName;
	
	private int price;
	
	private int quantity;
	
	
	
	private int categoryid;
	public int getCategoryid() {
		return categoryid;
}
	public void setCategoryid(int categoryid) {
	this.categoryid = categoryid;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public static  ProductDTO transferObject(Product product) {
		ModelMapper modelmapper = new ModelMapper();
		return modelmapper.map(product, ProductDTO.class);
	}
	
	public static Product tranferEntity(ProductDTO productDTO) {
		ModelMapper modelmapper = new ModelMapper();
		return modelmapper.map(productDTO, Product.class);
	}
	
	
}
