package com.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



import com.DAO.ProductOrderDAO;
import com.DTO.CategoryDTO;
import com.DTO.OderDTO;
import com.DTO.ProductOderDTO;
import com.model.Category;
import com.model.Oder;
import com.model.ProductOder;

@Service
public class ProductOderService {
	@Autowired
	private ProductOrderDAO proOderDAO;
	@Autowired
	private OderService orderService;
	
	public List<ProductOderDTO> getOrder(int id){
		return proOderDAO.findByOderId(id);
	}
	
	public ProductOder saveProductOrder(int productId, int oderId, String productName, int quantity, int price) {
		
		return proOderDAO.save(productId, oderId, productName, quantity, price);
	}
	
	public List<ProductOderDTO> getProductOder(int userId){
		List<OderDTO> oder= orderService.finByUserId(userId);
		List<ProductOderDTO> ProductOder = proOderDAO.findByOderId(oder.get(oder.size()).getId());
		return ProductOder;
		
	}
	

	public ProductOder save(ProductOder pro) {
		return proOderDAO.save(pro);
	}
	

	
}
