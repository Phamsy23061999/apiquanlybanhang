package com.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.DAO.OrderDAO;
import com.DTO.CategoryDTO;
import com.DTO.OderDTO;
import com.Repository.OderRepository;
import com.model.Category;
import com.model.Oder;
import com.model.ProductOder;

import Request.OrderRequest;

@Service
public class OderService {
	@Autowired
	private OrderDAO orders;
	@Autowired
	private ProductOderService proOrderService;


	public Oder createNewOrder(OrderRequest request) {
		Oder order = orders.save(request.getUserId());
		ProductOder pro = proOrderService.saveProductOrder(request.getProductId(), order.getId(), request.getProductName()
				, request.getQuantity(), request.getPrice());
		return order;
	}
	
	
	
	public List<OderDTO> finByUserId(int userId) {
		return orders.findByUser(userId);
	}
	
	public List<OderDTO> getAllODer(){
		return  orders.findAll();
	}
	
	

	
}
