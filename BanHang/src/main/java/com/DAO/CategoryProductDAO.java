package com.DAO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.DTO.CategoryDTO;
import com.DTO.CategoryProductDTO;
import com.DTO.ProductDTO;
import com.Repository.CategoryProductReposity;
import com.Repository.CategoryRepository;

import com.model.Category;
import com.model.CategoryProduct;
import com.model.Oder;
import com.model.Product;

@Component
public class CategoryProductDAO {
	@Autowired
	CategoryProductReposity categoryProductReposity;
	
	@Autowired
	CategoryRepository categoryReposity;
	
	
	
	public Product saveCategoryId(int id) {
		 Product pro = new Product();
		 pro.setCategoryid(id);
		 return categoryProductReposity.saveAndFlush(pro);
	}
	
	 
	
	public Product save (ProductDTO product){
		Category category = categoryReposity.findById(product.getCategoryid()).orElse(null);
		if(category != null) {
			Product pro = ProductDTO.tranferEntity(product);
			return categoryProductReposity.saveAndFlush(pro);	
		}
		
		return null;
		
	}
	
	public CategoryDTO findById(int id) {
		Category cate = categoryReposity.findById(id).get();
		return CategoryDTO.transferObject(cate);
	}

	
	
	
}
