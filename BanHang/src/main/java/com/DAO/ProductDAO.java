package com.DAO;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.DTO.CategoryDTO;
import com.DTO.ProductDTO;
import com.Repository.ProductRepository;
import com.model.Category;
import com.model.Product;

@Component
public class ProductDAO {
	
	@Autowired
	private ProductRepository repository;
	
	public List<ProductDTO> findProductId(int id){
		List<Product> pros = repository.findByCategoryId(id);
		List<ProductDTO> proRes= new ArrayList<ProductDTO>();
		for(Product pro: pros) {
			ProductDTO proDTO =  ProductDTO.transferObject(pro);
			proRes.add(proDTO);
		}
		return proRes;
		
	}

	public List<ProductDTO> findAll() {
		List<Product> product = repository.findAll();
		List<ProductDTO> lists= new ArrayList<ProductDTO>();
		for(Product pro: product) {
			ProductDTO proDTO =  ProductDTO.transferObject(pro);
			lists.add(proDTO);
		}
		
		return lists;
	}
	
	
	public List<Product> findByProductnameAndPrice(String productname, int price){
		return repository.findByProductnameAndPrice(productname, price);
		
	}
	
	public Product finByProductname(String productName) {
		return repository.findByProductname(productName);
	}
	
} 
