package com.DAO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.DTO.ProductOderDTO;
import com.Repository.ProductOderRepository;
import com.model.ProductOder;

@Component
public class ProductOrderDAO {

	@Autowired
	ProductOderRepository reposity;
	public List<ProductOderDTO> findByOderId(int id) {
		
		return reposity.findByOderId(id);
	}
	public ProductOder save(int productId, int oderId, String productName, int quantity, int price) {
		ProductOder pro = new ProductOder();
		pro.setOderId(oderId);
		pro.setProductId(productId);
		pro.setProductName(productName);
		pro.setQuantity(quantity);
		pro.setPrice(price);
		return reposity.saveAndFlush(pro);
	}
	public ProductOder save(ProductOder pro) {
		
		return reposity.save(pro);
	}
	
	

}
