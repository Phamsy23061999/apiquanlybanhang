package com.DAO;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.DTO.CategoryDTO;
import com.DTO.OderDTO;
import com.Repository.CategoryRepository;
import com.Repository.OderRepository;
import com.model.Category;
import com.model.Oder;

@Component
public class OrderDAO {
	@Autowired
	private OderRepository repository;
	
	public Oder save(int id) {
		Oder orders = new Oder();
		orders.setUserId(id);
		return repository.saveAndFlush(orders);
	}
	
	public OderDTO findByUserId(int userId) {
		Oder oders = repository.findByUserId(userId).get(userId);
		return OderDTO.transferObject(oders);
	}
	
	public List<OderDTO> findByUser(int userId) {
		
		List<Oder> oders = repository.findByUserId(userId);
		List<OderDTO> lists= new ArrayList<OderDTO>();
		for(Oder order: oders) {
			OderDTO orderDTO =  OderDTO.transferObject(order);
			lists.add(orderDTO);
		}
		return lists;
	}
	
	
	public List<OderDTO> findAll() {
		List<Oder> orders = repository.findAll();
		List<OderDTO> lists= new ArrayList<OderDTO>();
		for(Oder order: orders) {
			OderDTO orderDTO =  OderDTO.transferObject(order);
			lists.add(orderDTO);
		}
	
		return lists;
	}
	
	

}
