package com.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{

	List<Product> findByCategoryId(int id);
	
	 @Query("SELECT e FROM product e WHERE e.productname = :productname AND e.price = :price")
	  List<Product> findByProductnameAndPrice(@Param("productname") String productname, @Param("price") int price);
	 
	 
	 @Query("SELECT e FROM product e WHERE e.productname = :productname")
	  Product findByProductname(@Param("productname") String productname);


}
