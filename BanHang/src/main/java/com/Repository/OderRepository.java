package com.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.Category;
import com.model.Oder;

@Repository
public interface OderRepository extends JpaRepository<Oder, Integer> {

//	Optional<Oder> findByUserId(int userId);
	
	List<Oder> findByUserId(int userId);
	
	

	
}
